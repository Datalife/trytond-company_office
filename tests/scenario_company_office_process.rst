==============
Company Office
==============

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard


Install company office Module::

    >>> config = activate_modules(['company_office', 'productive_process'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create productive process::

    >>> Process = Model.get('productive.process')

    >>> process = Process()
    >>> process.name = 'Process 1'
    >>> process.save()

Check process of company office::

    >>> Office = Model.get('company.office')

    >>> office = Office()
    >>> office.name = 'Office 1'
    >>> office.process = process
    >>> office.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Process" in "Company branch office" is not valid according to its domain. - 
    >>> office.process = None
    >>> office.save()