# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
import doctest
from trytond.tests.test_tryton import doctest_teardown, doctest_checker
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.transaction import Transaction
from trytond.pool import Pool


def create_office(name='Office 1'):
    pool = Pool()
    Office = pool.get('company.office')

    office, = Office.create([{
        'name': name,
        }])
    return office


class CompanyOfficeTestCase(ModuleTestCase):
    """Test Company Office module"""
    module = 'company_office'

    @with_transaction()
    def test_user_office(self):
        'Test user company'
        pool = Pool()
        User = pool.get('res.user')
        Party = pool.get('party.party')

        office1 = create_office()
        office2 = create_office('Office 2')

        user = User(Transaction().user)
        User.write([user], {
            'offices': [('add', [office1.id, office2.id])],
            'office': office1.id,
        })
        user = User(user.id)

        user1, user2 = User.create([
            {
                'name': 'Jim Halper',
                'login': 'jim',
                'offices': [('add', [office1.id, office2.id])],
                'office': office1.id,
            }, {
                'name': 'Pam Beesly',
                'login': 'pam',
                'offices': [('add', [office2.id])],
                'office': office2.id
            }])
        self.assertTrue(user1)

        party1, party2 = Party.create([
            {
                'name': 'Customer 1',
                'offices': [('add', [office1.id])],
            }, {
                'name': 'Customer 2',
                'offices': [('add', [office2.id])],
            }
        ])
        self.assertEqual(list(map(int, party1.offices)), [office1.id])
        self.assertEqual(list(map(int, party2.offices)), [office2.id])
        self.assertEqual(sorted(list(map(int, user1.offices))),
            [office1.id, office2.id])
        self.assertEqual(list(map(int, user2.offices)), [office2.id])
        transaction = Transaction()
        with transaction.set_user(user2.id), \
                transaction.set_context(user=user2.id, _check_access=True), \
                transaction.set_context(User.get_preferences(
                    context_only=True)):
            self.assertEqual(
                Party.search([('id', '=', party1.id)]), [])
            self.assertEqual(
                Party.search([('id', '=', party2.id)]), [party2])


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            CompanyOfficeTestCase))
    suite.addTests(doctest.DocFileSuite(
            'scenario_company_office_process.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
