# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction


class Rule(metaclass=PoolMeta):
    __name__ = 'ir.rule'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.domain.help += '\n- "offices" from the current user'

    @classmethod
    def _get_context(cls):
        pool = Pool()
        User = pool.get('res.user')

        context = super()._get_context()
        # Use root to avoid infinite loop when accessing user attributes
        user_id = Transaction().user
        with Transaction().set_user(0):
            user = User(user_id)
        context['offices'] = [c.id for c in user.offices]
        return context
