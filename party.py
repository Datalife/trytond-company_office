# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from .office import OfficeRelationMixin

__all__ = ['Party', 'PartyOffice']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    offices = fields.Many2Many('party.party-company.office', 'party',
        'office', 'Offices', states={'readonly': ~Eval('active')},
        depends=['active'])
    #todo: add_remove only offices of user


class PartyOffice(OfficeRelationMixin, ModelSQL):
    """Party - Company Branch office"""
    __name__ = 'party.party-company.office'

    party = fields.Many2One('party.party', 'Party', required=True,
        select=True, ondelete='CASCADE')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._relation_model_field = 'party'
