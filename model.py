# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import MultiValueMixin, ValueMixin, fields
from trytond.transaction import Transaction

__all__ = ['OfficeMultiValueMixin', 'OfficeValueMixin']


class OfficeMultiValueMixin(MultiValueMixin):
    def get_multivalue(self, name, **pattern):
        Value = self.multivalue_model(name)
        if issubclass(Value, OfficeValueMixin):
            pattern.setdefault('office', Transaction().context.get('office'))
        return super().get_multivalue(
            name, **pattern)

    def set_multivalue(self, name, value, **pattern):
        Value = self.multivalue_model(name)
        if issubclass(Value, OfficeValueMixin):
            pattern.setdefault('office', Transaction().context.get('office'))
        return super().set_multivalue(
            name, value, **pattern)


class OfficeValueMixin(ValueMixin):
    _office_required = True
    office = fields.Many2One('company.office', 'Office', select=True,
        ondelete='CASCADE')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        if cls._office_required:
            cls.office.required = True
