datalife_company_office
=======================

The company_office module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-company_office/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-company_office)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
