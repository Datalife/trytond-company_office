# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields, DeactivableMixin
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, If
from trytond.transaction import Transaction
from trytond.cache import Cache
from trytond.i18n import gettext
from trytond.exceptions import UserError
from itertools import groupby

__all__ = ['Office', 'User', 'UserOffice']


class Office(ModelSQL, ModelView, DeactivableMixin):
    """Company branch office"""
    __name__ = 'company.office'

    name = fields.Char('Name', select=True,
        required=True, states={'readonly': ~Eval('active')},
        depends=['active'])
    company = fields.Many2One('company.company', 'Company',
        select=True, states={'readonly': ~Eval('active')},
        depends=['active'])
    users = fields.Many2Many('res.user-company.office', 'office',
        'user', 'Users', domain=[
            If(Eval('company'),
                ('companies', '=', Eval('company')),
                ()
            )],
        depends=['company'])


class User(metaclass=PoolMeta):
    __name__ = 'res.user'

    office = fields.Many2One('company.office', 'Current Office',
        domain=[('id', 'in', Eval('offices'))],
        depends=['offices'],
        help="Select the branch office to work for.")
    offices = fields.Many2Many('res.user-company.office', 'user',
        'office', 'Offices', domain=[If(Eval('active'), ['OR',
            ('company', 'in', Eval('companies')),
            ('company', '=', None)], [])],
        depends=['active', 'companies'])

    @classmethod
    def __setup__(cls):
        super(User, cls).__setup__()
        cls._preferences_fields.append('office')
        cls._context_fields.insert(0, 'offices')

    @classmethod
    def _get_preferences(cls, user, context_only=False):
        res = super(User, cls)._get_preferences(user,
            context_only=context_only)
        res['offices'] = [o.id for o in user.offices]
        if user.office:
            res['office'] = user.office.id
            res['office.rec_name'] = user.office.rec_name
        return res

    def get_status_bar(self, name):
        status = super(User, self).get_status_bar(name)
        if self.office:
            status += ' - %s' % self.office.rec_name
        return status


class UserOffice(ModelSQL):
    """User - Company branch office"""
    __name__ = 'res.user-company.office'

    office = fields.Many2One('company.office', 'Office',
        ondelete='RESTRICT', select=True, required=True)
    user = fields.Many2One('res.user', 'User', ondelete='CASCADE',
        select=True, required=True)


class OfficeRelationMixin(object):
    office = fields.Many2One('company.office', 'Office', required=True,
        select=True, ondelete='RESTRICT')
    _relation_model_field = ''

    @classmethod
    def create(cls, vlist):
        records = super().create(vlist)
        relation_records = set(getattr(r, cls._relation_model_field)
            for r in records)
        for relation_record in relation_records:
            for model, office_field, field in cls._get_models_to_check():
                Model = Pool().get(model)
                found = Model.search([
                    (office_field, '!=', None),
                    (office_field, 'not in', relation_record.offices),
                    (field, '=', relation_record)], limit=1)
                if found:
                    found, = found
                    IrModel = Pool().get('ir.model')
                    ir_model, = IrModel.search([('model', '=', model)])
                    relation_model, = IrModel.search([
                        ('model', '=', relation_record.__name__)])
                    raise UserError(gettext(
                        'company_office.msg_missing_office',
                        office=found.office.rec_name,
                        relation_model=relation_model.rec_name,
                        relation_record=getattr(found,
                            field.split('.', 1)[0]).rec_name,
                        model=ir_model.rec_name,
                        record=found.rec_name))
        return records

    @classmethod
    def delete(cls, records):
        sorted_records = sorted(records,
            key=lambda x: getattr(x, cls._relation_model_field))
        for relation_record, grouped_records in groupby(sorted_records,
                key=lambda x: getattr(x, cls._relation_model_field)):
            grouped_records = list(grouped_records)
            relation_records = list(
                set(getattr(r, cls._relation_model_field)
                for r in records))
            other_records = cls.search([
                    (cls._relation_model_field, '=', relation_record),
                    ('id', 'not in', list(map(int, grouped_records)))],
                limit=1)
            if (not other_records
                    and len(grouped_records) == len(relation_record.offices)):
                continue
            offices = list(set(r.office for r in grouped_records))
            for model, office_field, field in cls._get_models_to_check():
                Model = Pool().get(model)
                found = Model.search([
                    (office_field, 'in', offices),
                    (field, '=', relation_record)], limit=1)
                if found:
                    found, = found
                    IrModel = Pool().get('ir.model')
                    ir_model, = IrModel.search([('model', '=', model)])
                    relation_model, = IrModel.search([
                        ('model', '=', relation_records[0].__name__)])
                    raise UserError(gettext(
                        'company_office.msg_field_found',
                        office=offices[0].rec_name,
                        relation_model=relation_model.rec_name,
                        relation_record=getattr(found,
                            field.split('.', 1)[0]).rec_name,
                        model=ir_model.rec_name,
                        record=found.rec_name))

        super().delete(records)

    @classmethod
    def _get_models_to_check(cls):
        return []


class OfficeMixin(object):

    office = fields.Many2One('company.office', 'Office',
        domain=[
            If(Eval('state') == 'draft', [
                ('id', 'in', Eval('context', {}).get('offices', [])),
                ['OR',
                    ('company', '=', None),
                    ('company', '=', Eval('company'))
                ]], [])
        ],
        states={'readonly': Eval('state') != 'draft'},
        depends=['state', 'company'])

    @classmethod
    def default_office(cls):
        return Transaction().context.get('office')


class Office2(metaclass=PoolMeta):
    __name__ = 'company.office'

    process = fields.Many2One('productive.process', 'Process',
        domain=[('id', 'in', Eval('allowed_processes'))],
        depends=['allowed_processes'])
    allowed_processes = fields.Function(fields.Many2Many(
        'productive.process', None, None, 'Allowed processes'),
        'on_change_with_allowed_processes')
    _allowed_processes_cache = Cache('company_office.allowed_processes')

    @fields.depends('active')
    def on_change_with_allowed_processes(self, name=None):
        pool = Pool()
        Model = pool.get('company.office')
        ModelData = pool.get('ir.model.data')

        if not self._allowed_processes_cache.get(0):
            processes = [ModelData.get_id(module, xml_id)
                for module, xml_id in Model._get_allowed_processes()]
            self._allowed_processes_cache.set(0, processes)
        return self._allowed_processes_cache.get(0)

    @classmethod
    def get_allowed_processes(cls, records, name):
        values = cls._allowed_processes_cache.get(0)
        if not values:
            values = cls.on_change_with_allowed_processes()
        return {r.id: values for r in records}

    @classmethod
    def _get_allowed_processes(cls):
        """Returns list of tuple with (module, xml_id) of model data"""
        return []
