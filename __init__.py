# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import office
from . import party
from . import ir


def register():
    Pool.register(
        office.Office,
        office.User,
        office.UserOffice,
        party.Party,
        party.PartyOffice,
        ir.Rule,
        module='company_office', type_='model')
    Pool.register(
        office.Office2,
        module='company_office', type_='model',
        depends=['productive_process'])
